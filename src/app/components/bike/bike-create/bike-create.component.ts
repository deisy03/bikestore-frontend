import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BikeService} from '../bike.service';
import { from } from 'rxjs';
import { Router } from '@angular/router';
@Component({
  selector: 'app-bike-create',
  templateUrl: './bike-create.component.html',
  styleUrls: ['./bike-create.component.css']
})
export class BikeCreateComponent implements OnInit {

  bikeFormGroup: FormGroup;
  constructor(private formBuilder: FormBuilder,
              private bikeService: BikeService,
              private router: Router
  ) {
this.bikeFormGroup = this.formBuilder.group({
  model: ['', [ Validators.required, Validators.maxLength(4)]],
  serial: [''],
  price: ['']
});
   }

  ngOnInit() {
  }
saveBike() {
console.warn('DATOS', this.bikeFormGroup.value);
this.bikeService.saveBike(this.bikeFormGroup.value)
.subscribe( res => {
console.warn ('save OK', res);
this.router.navigate(['./bikes/bike-list']);
}, error => {
  console.warn('Error', error);
});
}
}
