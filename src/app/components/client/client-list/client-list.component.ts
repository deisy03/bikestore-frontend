import { Component, OnInit } from '@angular/core';
import { IClient } from '../client';
import { ClientService } from '../client.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {

  clientlist: IClient[];

  constructor(private clientService: ClientService) { }

  ngOnInit() {
    this.clientService.query()
    .subscribe(res => {
      console.log('Get Data', res);
      this.clientlist = res;
    }, error => {
    console.error( 'Error ', error);
  });
  }

}
