export interface IClient {
    id?: number;
    name: string;
    email: string;
    phoneNumber: string;
    documentNumber: string;
}

export class Client implements IClient {
    id?: number;
    name: string;
    email: string;
    phoneNumber: string;
    documentNumber: string;
}

