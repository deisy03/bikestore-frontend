import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalesListComponent } from './sales-list/sales-list.component';
import { SalesCreateComponent } from './sales-create/sales-create.component';
import { SalesViewComponent } from './sales-view/sales-view.component';
import { SalesUpdateComponent } from './sales-update/sales-update.component';
import { SalesRoutingModule } from './sales-routing.module';
import { CatalogoComponent } from './catalogo/catalogo.component';



@NgModule({
  declarations: [SalesListComponent, SalesCreateComponent, SalesViewComponent, SalesUpdateComponent, CatalogoComponent],
  imports: [
    CommonModule,
    SalesRoutingModule
  ]
})
export class SalesModule { }
